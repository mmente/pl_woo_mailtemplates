<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://polylook.de
 * @since      1.0.0
 *
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
