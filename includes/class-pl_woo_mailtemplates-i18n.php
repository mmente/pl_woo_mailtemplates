<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://polylook.de
 * @since      1.0.0
 *
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/includes
 * @author     Polylook <mente@polylook.de>
 */
class Pl_woo_mailtemplates_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'pl_woo_mailtemplates',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
