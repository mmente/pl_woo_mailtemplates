<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://polylook.de
 * @since      1.0.0
 *
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/includes
 * @author     Polylook <mente@polylook.de>
 */
class Pl_woo_mailtemplates_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
