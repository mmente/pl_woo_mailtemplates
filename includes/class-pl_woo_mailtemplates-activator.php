<?php

/**
 * Fired during plugin activation
 *
 * @link       http://polylook.de
 * @since      1.0.0
 *
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pl_woo_mailtemplates
 * @subpackage Pl_woo_mailtemplates/includes
 * @author     Polylook <mente@polylook.de>
 */
class Pl_woo_mailtemplates_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
