<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://polylook.de
 * @since             1.0.0
 * @package           Pl_woo_mailtemplates
 *
 * @wordpress-plugin
 * Plugin Name:       pl_woo_mailtemplates
 * Plugin URI:        http://polylook.de
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.2
 * Author:            Polylook
 * Author URI:        http://polylook.de
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pl_woo_mailtemplates
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pl_woo_mailtemplates-activator.php
 */
function activate_pl_woo_mailtemplates() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pl_woo_mailtemplates-activator.php';
	Pl_woo_mailtemplates_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pl_woo_mailtemplates-deactivator.php
 */
function deactivate_pl_woo_mailtemplates() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pl_woo_mailtemplates-deactivator.php';
	Pl_woo_mailtemplates_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pl_woo_mailtemplates' );
register_deactivation_hook( __FILE__, 'deactivate_pl_woo_mailtemplates' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pl_woo_mailtemplates.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pl_woo_mailtemplates() {

	$plugin = new Pl_woo_mailtemplates();
	$plugin->run();

}

//run_pl_woo_mailtemplates();

function myplugin_plugin_path() {

	return untrailingslashit( plugin_dir_path( __FILE__ ) );

}


add_filter( 'woocommerce_locate_template', 'myplugin_woocommerce_locate_template', 10, 3 );

function myplugin_woocommerce_locate_template( $template, $template_name, $template_path ) {

	global $woocommerce;

	$_template = $template;

	if ( ! $template_path )
	{
		$template_path = $woocommerce->template_url;
	}

	$plugin_path  = myplugin_plugin_path() . '/woocommerce/';

	// Look within passed path within the theme - this is priority
	$template = locate_template(
		array(
			$template_path . $template_name,
			$template_name
		)
	);

	if (  file_exists( $plugin_path . $template_name ) ){
		$template = $plugin_path . $template_name;
	}

	if ( ! $template )
		$template = $_template;

	return $template;

}

function filter_woocommerce_email_order_items_args( $array ) {

	foreach ( $array['order']->get_data()['line_items'] as $key => $value ){


		if( $value->get_variation_id() && get_field('colorit') ){

		}
//            print_r( get_class_methods($value) );

		$parent = wc_get_product($value->get_product_id());

		if ( $parent->is_type('simple') ){


		} else {

			$data = $value->get_data();
			$name = '';
			foreach ( $data['meta_data'] as $key => $valuemeta ){
				if ($key == 'pa_sample'){
					if ($valuemeta == 'sample'){
						$name = ' MUSTER: ' ;
					}
				}
			}
			$name .= $parent->get_name() .' ' ;
			$value->set_name( $name );

		}

	}

	return $array;
};

add_filter( 'woocommerce_email_order_items_args', 'filter_woocommerce_email_order_items_args', 10, 1 );


function filter_woocommerce_display_item_meta( $html, $item = '', $args = array() ) {

	//print_r($html);die();
	return '';

};

add_filter( 'woocommerce_display_item_meta', 'filter_woocommerce_display_item_meta', 10, 1 );

